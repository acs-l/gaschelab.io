% Some new things since 4.02.1
% Gabriel Scherer
% OCaml Paris Meeting, June 3st

I made an OUPS talk on May 22, 2014. Some quotes:

> I made an OUPS talk on May 21, 2013. Some quotes:
> 
> > We already do have a lot of external contributors on the bugtracker!
> 
> > Breaking changes are not accepted.
> 
> > Bugfixes and contributions for OCamlbuild are desirable and welcome.
> 
> > review proposed patches: read the code to give an opinion, improve
> > on the implementation, or test them against your own software

## Today (June 4, 2015)

A review of some of the user-facing changes since 4.02.1.

Most in trunk (4.02.3+dev), some in 4.02 (4.02.2+dev).

Please ask questions,

but I don't have all the answers.

## Custom index operators (trunk)

GPR#69: Custom index operators: `( .() )`, `( .[] )`, `( .{} )` etc.

(user 'Octachron')

The syntax `foo.(bar) <- baz` now desugars into `( .()<- ) foo bar bar`.
This should allow user to define their own notations by overriding.

    open Cute_hash_tables
    
    let test = hash [("foo", 1); ("bar", 2)]
    let () =
      print_int test.{"foo"};
      test.{"bar"} <- 3;
      print_int test.{"bar"};
    ()

## Tail call annotation (trunk)

GPR#111: `(f [@taillcall]) x y` warns if `f x y` is not a tail-call

(Simon Cruanes)

    type 'a tree =
      | Node of 'a tree * 'a * 'a tree
      | Leaf
    
    let to_list tree =
      let rec list tree acc = match tree with
        | Leaf -> acc
        | Node (left, x, right) ->
           (list [@tailcall]) left (x :: list right acc)
      in list tree []

## result type (trunk)

GPR#147: `type 'a result = Ok of 'a | Error of 'b` in Pervasives

(Yaron Minsky)

    type 'a result = Ok of 'a | Error of 'b

##

PR#4714, PR#5960, GPR#151, GPR#166

. . .

    List.cons : 'a -> 'a list -> 'a list

## Breaking in 4.02

PR#6016: add a `nonrec` keyword for type declarations

(Jérémie Dimino)

    type t
    module M = struct
      type nonrec t = t
    end

## Some work on ASCII vs. unicode

PR#6694, PR#6695: deprecate functions using ISO-8859-1 character set
in `Char`, `Bytes`, `String` and provide alternatives using US-ASCII.

(Peter Zotov)

    val capitalize : string -> string
      [@@ocaml.deprecated
        "Use String.capitalize_ascii instead."]
    
    val capitalize_ascii : string -> string    

PR#6521: `{Bytes,Char,String}.escaped` are locale-dependent

(Damien Doligez, report by Jun Furuse)

## Lots of cross-compilation work

Mostly Peter "whitequark" Zotov.

PR#6845: -no-check-prims to tell ocamlc not to check primitives in runtime

PR#6794, PR#6809: ocamlbuild: pass package-specific include flags when building C files
(Jérémie Dimino, request by Peter Zotov)

PR#5887: move the byterun/*.h headers to byterun/caml/*.h to avoid header
  name clashes
(Jérôme Vouillon and Adrien Nader and Peter Zotov)

PR#6642: replace $CAMLORIGIN in -ccopt with the path to cma or cmxa
(Peter Zotov, Gabriel Scherer, review by Damien Doligez)

PR#6845: -no-check-prims to tell ocamlc not to check primitives in runtime
(Alain Frisch)

##

PR#6625: ocamlbuild: pass -linkpkg to files built with -output-obj.
(Peter Zotov)

PR#6702: ocamlbuild: explicit "linkpkg" and "dontlink(foo)" flags
(Peter Zotov, Gabriel Scherer)

PR#6720: ocamlbuild: pass -g to C compilers when tag 'debug' is set
(Peter Zotov, Gabriel Scherer)

PR#6733: ocamlbuild: add .byte.so and .native.so targets to pass
-output-obj -cclib -shared.
(Peter Zotov)

PR#6733: ocamlbuild: "runtime_variant(X)" to pass -runtime-variant X option.
(Peter Zotov)

PR#5887: move the byterun/*.h headers to byterun/caml/*.h to avoid
header name clashes

PR#6616: allow meaningful use of -use-runtime without -custom.
(Peter Zotov)

##

PR#6617: allow android build with pthreads support (since SDK r10c)
(Peter Zotov)

PR#6693 (part two): Incorrect relocation types in x86-64 runtime system
(Peter Zotov, review by Jacques-Henri Jourdan, Xavier Leroy and Mark Shinwell)

PR#6641: add -g, -ocamlcflags, -ocamloptflags options to ocamlmklib
(Peter Zotov)

PR#6693: also build libasmrun_shared.so and lib{asm,caml}run_pic.a
(Peter Zotov, review by Mark Shinwell)

##

Android cross-compilation should be working about ok.

> iOS cross-compilation support may be coming in 4.03.

## Toplevel and debugger

A line of work on improving bytecode toplevel and debugger.

(some of it mandated by the Coq team)

PR#6760: closures evaluated in the toplevel can now be marshalled
(Peter Zotov, review by Jacques-Henri Jourdan)

PR#6468: toplevel now supports backtraces if invoked with OCAMLRUNPARAM=b
(Peter Zotov and Jake Donham,
 review by Gabriel Scherer and Jacques-Henri Jourdan)

PR#5958: generalized polymorphic #install_printer
(Pierre Chambart and Grégoire Henry)

PR#5836, PR#6684: printing lazy values in ocamldebug may segfault
(Gabriel Scherer, request by the Coq team)

PR#6669: fix 4.02 regression in toplevel printing of lazy values
(Leo White, review by Gabriel Scherer)

##

    #
      let pp_list_funky li =
        let pp_sep ppf () =
          Format.fprintf ppf " then " in
        Format.pp_print_list ~pp_sep li;;
        
    - val pp_list_funky :
        (Format.formatter -> 'a -> unit) ->
          Format.formatter -> 'a list -> unit
        
    #
      #install_printer pp_list_funky;;

    #
      [1; 2; 3];;
    - : int list = 1 then 2 then 3

> debugging dynlinked code may be coming for 4.03

## Easier external tooling

PR#6611: remove the option wrapper on optional arguments in the syntax tree
(Alain Frisch, review by Damien Doligez and Jacques Garrigue, request by Peter Zotov)

PR#6691: install .cmt[i] files for stdlib and compiler-libs
(David Sheets, request by Gabriel Radanne)

PR#6816: reject integer and float literals followed by alphanum
("3foo" was previously parsed as "3 foo" so you could write,
for example, "cons 3xs"; this is now rejected by the lexer)
(Hugo Heuzard)

GPR#137: add untypeast.ml (in open recursion style) to compiler-libs
(Gabriel Radanne)

GPR#171: allow custom warning printers / catchers
(Benjamin Canou, review by Damien Doligez)


## Various optimization work

GPR#89: improve type-specialization of unapplied primitives
(Frédéric Bour, review by Gabriel Scherer)

GPR#17: some cmm optimizations of integer operations with constants
(Stephen Dolan, review by Pierre Chambart)

PR#6577: improve performance of %L, %l, %n, %S, %C format specifiers
(Alain Frisch)

PR#6645, GPR#174: Guarantee that Set.add, Set.remove, Set.filter
return the original set if no change is required
(Alain Frisch, Mohamed Iguernelala)

GPR#145: speeedup bigarray access by optimizing Cmmgen.bigarray_indexing
(Vladimir Brankov, review by Gabriel Scherer)

> flambda may be coming to 4.03

##

About 263 issues submitted since 4.02.1 was released.

Total: 73 features (28% total), 189 bugs (72% total)

Still open: 50 features (68% features), 66 bugs (34% all bugs)

I think we're doing a good job at fixing bugs.

##

  Adrien Nader,
  Alain Frisch,
  Andi McClure,
  Andreas Hauptmann,
  Andrew Ray,
  Anil Madhavapeddy,
  Antoine Miné,
  Benjamin Canou,
  Benoît Vaugon,
  Berke Durak,
  Jean-Christophe Filliâtre,
  Christophe Troestler,
  Damien Doligez,
  Daniel Bünzli,
  Daniel Weil,
  darktenaibre,
  David Allsopp,
  David Sheets,
  dhekir,
  Didier Le Botlan,
  Eric Cooper,
  Eyyüb Sari,
  François Pottier,
  Frédéric Bour,
  Gabor Pali,
  Gabriel Radanne,
  Gabriel Scherer,
  Gerd Stolpmann,
  Gergely Szilvasy,
  gfxmonk,
  Grégoire Henry,
  Hugo Heuzard,
  Jacques Garrigue,
  Jacques-Henri Jourdan,
  Jake Donham,
  Jérémie Dimino,
  Jeremy Yallop,
  Jérôme Vouillon,
  John Tibble,
  Jordan Walke,
  Jun Furuse,
  Kenji Tokudome,
  Kyle Headley,
  Leo White,
  Mark Shinwell,
  Markus Mottl,
  maro,
  Maverick Woo,
  Maxime Dénès,
  Michael Grünewald,
  Mickaël Delahaye,
  Mikhail Mandrykin,
  Mohamed Iguernelala,
  Nicolas Braud,
  Octachron,
  Pascal Deplaix,
  Peter Zotov,
  Philippe Veber,
  Pierre Chambart,
  Rolf Rolles,
  Romain Beauxis,
  Romain Calascibetta,
  Roshan James,
  Simon Cruanes,
  Stéphane Glondu,
  Stephen Dolan,
  Stephen Weeks,
  Thomas Leonard,
  Valentin Gatien-Baron,
  Vassili Karpov,
  Vincent Laporte,
  Vladimir Brankov,
  william,
  Xavier Leroy,
  Yaron Minsky


<!--

Note: the "ocaml" syntax highlighter of pandoc fails with an error on
my machine, so I use "haskell" instead. To get correct highlighting
I then have to replace the single-quote character ' with its UTF8
variant ’.

  cat ocaml-paris-meeting-june-2015-scherer.text\
  | sed "s/'/’/g"\
  | sed -e "s,PR#\\([0-9][0-9][0-9][0-9]\\),[PR#\\1](http://caml.inria.fr/mantis/view.php?id=\\1),g"\
  | sed -e "s,GPR#\\([0-9][0-9][0-9]\\),[GPR#\\1](https://github.com/ocaml/ocaml/pull/\\1),g"\
  | pandoc -s --mathml -i -t dzslides --indented-code-classes haskell\
  | sed "s/font-size: 20pt;/font-size: 18pt;/g"\
  | sed "s/color: red;//g"\
  > ocaml-paris-meeting-june-2015-scherer.html

-->

