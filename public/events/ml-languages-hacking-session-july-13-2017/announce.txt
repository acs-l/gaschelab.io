# ML languages hacking evening in Pittsburgh, PA, US, July 13th

Dear all,

We are happy to announce a hacking session for languages of the ML
family in the evening of July 13th in Pittsburgh, PA, US. The event
will take place at CMU, between 16:30 and 22:30. Access instructions
can be found at:

  http://www.ccs.neu.edu/home/gasche/tmp/ml-languages-hacking-session-july-13-2017/announce.html

This event, organized by Anton Bachin, Adrien Guatto, Ram Raghunathan,
Gabriel Scherer and Jon Sterling, is open to all programmers in
a ML-family language, advanced or beginners. Attendees will be
encouraged and assisted in making a contribution to an open source
project, including in particular the OCaml and MLton (SML) compiler
implementations -- we will propose a list of tasks and project ideas,
and try to help in providing technical advice, feedback and guidance
for contribution.

Coming with a project in mind is also welcome. There are other
impactful contribution than code: documentation contributions are
warmly welcome. If you were waiting for an opportunity to scratch an
ML-y itch in a friendly place, there it is!

Event implementation details are still being arranged, but we are not
planning on having food at the event itself. We may go out in groups
around dinner time, but feel free to eat beforehand or bring your own
food.
