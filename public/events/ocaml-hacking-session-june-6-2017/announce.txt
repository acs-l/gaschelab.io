# OCaml hacking evening in Cambridge, MA, *US*, June 6th.

Dear all,

I am happy to announce an OCaml hacking session in the evening of June
6th in Cambridge, MA, US. The event will take place at MIT, between
17:00 and 23:30, in the room 3-133. Access instructions can be found
at:

  http://www.ccs.neu.edu/home/gasche/tmp/ocaml-hacking-session-june-6-2017/announce.html

This event, organized by Clément Pit-Claudel, Thomas Bourgeat and
myself, is open to all OCaml programmers, advanced or
beginners. Attendees will be encouraged and assisted in making
a contribution to an OCaml open source project, including in
particular the OCaml compiler implementation itself -- we will propose
a list of tasks and project ideas, and try to help in providing
technical advice, feedback and guidance for contribution.

Coming with a project in mind is also welcome, on the compiler
codebase or any other OCaml codebase. Many blocks of the OCaml
ecosystem would benefit from contributions, including documentation
contributions and the https://ocaml.org website. If you were waiting
for an opportunity to scratch an OCaml-y itch in a friendly place,
there it is!

Event implementation details are still being arranged, but we are not
planning on having food at the event itself. We may go out in groups
around dinner time, but feel free to eat beforehand or bring your own
food.
