let rec append la lb = match la with
  | [] -> lb
  | a::ra -> a :: append ra lb

(* À montrer:

1.  ∀l, append l [] = l
2.  ∀ l r u, append l (append r u) = append (append l r) u
*)

let snoc l a = append l [a]

let rec rev = function
  | [] -> []
  | a :: ra -> snoc (rev ra) a

let rec rev_append la acc = match la with
  | [] -> acc
  | a::ra -> rev_append ra (a::acc)

let reva l = rev_append l []

(* À montrer:

3. ∀l, rev l = reva l 
4. ∀l r, rev (append l r) = append (rev r) (rev l)
5. ∀ l, reva (reva l) = l

   Remarque: la 5. est plus difficile que les autres questions,
   ne pas hésiter à passer aux suivantes.
*)

type 'a tree =
| Leaf of 'a
| Node of 'a tree * 'a tree

let rec revt = function
  | Leaf a -> Leaf a
  | Node (t, u) -> Node (revt u, revt t)

let rec leaves = function
  | Leav a -> [a]
  | Node (t, u) -> append (leaves t) (leaves u)

(* À montrer:

6. ∀ t, revt (revt t) = t
7. ∀ t, leaves (revt t) = rev (leaves t)
*)
