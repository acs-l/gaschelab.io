(* streams suspendus des le debut *)
type 'a stream = Nil | Cons of 'a * 'a strsusp
and 'a strsusp = 'a stream Lazy.t

(* test avec le factoriel *)
let rec fact = function 
  | 0 -> 1
  | n -> n * (fact (n-1))

let rec fact_from n = 
    lazy (Cons((Printf.eprintf "*%!";fact n), 
            lazy (Lazy.force (fact_from (n+1)))))

let rec take_list n s = match (n,s) with
      0,s -> [] 
    | n,s -> match (Lazy.force s) with Nil -> [] 
           | Cons(v,r) -> v :: (take_list (n-1) r)



(* le module Stream et la signature STREAM *)
module type STREAM = sig
  type 'a streamhead = Nil | Cons of 'a * 'a stream
  and 'a stream = 'a streamhead Lazy.t

  (* concatenation de streams *)
  val (++) : 'a stream -> 'a stream -> 'a stream

  (* un stream contenant les premiers n elements *)
  val take : int -> 'a stream -> 'a stream

  (* le stream sans les premiers n elements *)
  val drop : int -> 'a stream -> 'a stream
  val reverse : 'a stream -> 'a stream
end;;


let (!$) = Lazy.force (* abbreviation *)



module Stream : STREAM = struct
  type 'a streamhead = Nil | Cons of 'a * 'a stream
  and 'a stream = 'a streamhead Lazy.t

  (* concatenation *)
  let (++) s1 s2 = 
    let rec aux s = match !$s with
    | Nil -> !$s2
    | Cons (hd, tl) -> Cons (hd, lazy(aux tl))
  in lazy(aux s1)

  (* copie paresseuse des premier n elements *)
  let take n s = 
    let rec take' n s = match n, !$s with
    | 0, _ -> Nil
    | _, Nil -> Nil
    | _, Cons (hd, tl) -> Cons (hd, lazy (take' (n - 1) tl))
  in lazy(take' n s)

  (* copie du stream apres avoir n elements *)
  let drop n s =
    let rec drop' n s = 
      match n with 0 -> !$s
      | n -> match !$s with
             | Nil -> Nil
             | Cons (_, tl) -> (drop' (n - 1) tl) in
    lazy (drop' n s)

  (* retourne un stream, monolythique...! *)
  let reverse s =
    let rec reverse' acc s = match !$s with
      | Nil -> acc
      | Cons (hd, tl) -> 
           reverse' (Cons (hd, lazy acc)) tl in
    lazy (reverse' Nil s)

  let add x s = lazy (Cons(x,s))  
end


module type STREAMEXT = sig
  include module type of Stream
  val of_list : 'a list -> 'a stream
  val to_list : 'a stream -> 'a list 
  val push : 'a -> 'a stream -> 'a stream
  val pop : 'a stream -> ('a * 'a stream) option
  val map : ('a -> 'b) -> 'a stream  -> 'b stream
  val filter : ('a -> bool) -> 'a stream -> 'a stream
  val split : ('a * 'b) stream -> 'a stream * 'a stream
  val join : 'a stream * 'b stream -> ('a * 'b) stream
end


module Streamext : STREAMEXT = struct
  include Stream
  (* A compléter ! *)
end
