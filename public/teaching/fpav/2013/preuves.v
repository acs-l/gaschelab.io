Inductive list A :=
  | Nil : list A
  | Cons : A -> list A -> list A.

(* on demande à Coq d'inférer le type de la liste quand on utilise Nil et Cons *)
Arguments Nil [A].
Arguments Cons [A] _ _.

Fixpoint append {A} (la : list A) lb :=
  match la with
    | Nil => lb
    | Cons a ra => Cons a (append ra lb)
  end.

Lemma append_nil : forall A (l : list A), append l Nil = l.
  induction l.
  + auto.
  + simpl. rewrite IHl. auto.
Qed.

Lemma append_assoc : forall A (l r u : list A),
                       append l (append r u) = append (append l r) u.
  induction l.
  + auto.
  + simpl. intros. rewrite (IHl r u). auto.
Qed.

Definition snoc {A} (l : list A) a := append l (Cons a Nil).

Fixpoint rev {A} (la : list A) :=
  match la with
    | Nil => Nil
    | Cons a ra => snoc (rev ra) a
  end.

Fixpoint rev_append {A} (la : list A) acc :=
  match la with
    | Nil => acc
    | Cons a ra => rev_append ra (Cons a acc)
  end.
Definition reva {A} (l : list A) := rev_append l Nil.

Lemma rev_reva : forall A (l : list A), rev l = reva l.
  Lemma spec_rev_append : forall A (l r : list A),
                          rev_append l r = append (rev l) r.
    induction l.
    + auto.
    + simpl. intro r.
      unfold snoc.
      rewrite <- append_assoc. simpl.
      rewrite (IHl (Cons a r)).
      auto.
  Qed.
  intros A l.
  unfold reva.
  rewrite (spec_rev_append A l Nil).
  rewrite append_nil.
  auto.
Qed.

Lemma rev_on_append : forall A (l r : list A), rev (append l r) = append (rev r) (rev l).
  induction l; simpl; intros.
  + rewrite append_nil. auto.
  + unfold snoc.
    rewrite IHl.
    rewrite append_assoc.
    auto.
Qed.

(* plus difficile que les autres, vous pouvez l'éviter en première lecture *)
Lemma rev_twice : forall A (l : list A), reva (reva l) = l.
  Lemma rev_append_twice :
    forall A (l r u : list A), rev_append (rev_append l r) u = append (append (rev r) l) u.
    induction l; simpl; intros r u.
    + rewrite append_nil. apply spec_rev_append.
    + rewrite IHl. simpl. unfold snoc.
      assert (append (append (rev r) (Cons a Nil)) l = append (rev r) (Cons a l)) as H
        by (rewrite <- append_assoc; auto).
      rewrite H. auto.
  Qed.
  unfold reva.
  intros A l.
  rewrite (rev_append_twice A l Nil Nil).
  simpl.
  apply append_nil.
Qed.


Inductive tree A := 
| Leaf : A -> tree A
| Node : tree A -> tree A -> tree A.

Arguments Leaf [A] _.
Arguments Node [A] _ _.

Fixpoint revt {A} (t : tree A) :=
  match t with
    | Leaf a => Leaf a
    | Node t u => Node (revt u) (revt t)
  end.

Lemma revt_twice : forall A (t : tree A), revt (revt t) = t.
  induction t.
  + auto.
  + simpl. rewrite IHt1. rewrite IHt2. auto.
Qed.

Fixpoint leaves {A} (t : tree A) :=
  match t with
    | Leaf a => Cons a Nil
    | Node t u => append (leaves t) (leaves u)
  end.

Lemma revt_leaves : forall A (t : tree A), leaves (revt t) = rev (leaves t).
  induction t.
  + auto.
  + simpl. rewrite IHt1. rewrite IHt2.
    rewrite rev_on_append.
    auto.
Qed.
