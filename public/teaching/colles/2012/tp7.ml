#open "graphics";;
open_graph " 640x480";;

let xlen, ylen = 640, 480;;

let nettoyer () =
  set_color black;
  clear_graph ();;

(** Question 1 *)
let carre () =
  nettoyer ();
  for x = 0 to 255 do
    for y = 0 to 255 do
      set_color (rgb x y (255 - y));
      plot x y;
    done;
  done;;

(** Question 2 *)
let nuage n =
  let point () = 50 + random__int (xlen - 100), 50 + random__int (ylen - 100) in
  (* constantes ajoutées pour éviter les points trop au bord *)
  let rec nuage = function
  | 0 -> []
  | n -> point () :: nuage (n - 1) in
  nuage n;;

let dessine nuage =
  do_list (fun (x, y) -> plot x y) nuage;;

(** Question 3 *)
let rec dessine_couples dessin = function
| [] -> ()
| hd::tl -> do_list (dessin hd) tl; dessine_couples dessin tl;;

(** Question 4 *)
type direction = Gauche | Milieu | Droite;;

let vect (x, y) (x', y') = (x' - x, y' - y);;
let prod_vect (x, y) (x', y') = x * y' - x' * y;;

let position a b c =
  let prod = prod_vect (vect a b) (vect a c) in
  if prod > 0 then Gauche
  else if prod < 0 then Droite
  else Milieu;;

let repartition a b nuage =
  let gauche, milieu, droite = ref 0, ref 0, ref 0 in
  let compte c =
    incr (match position a b c with
          | Gauche -> gauche
          | Milieu -> milieu
          | Droite -> droite) in
  do_list compte nuage;
  (!gauche, !milieu, !droite);;

let segment (x, y) (x', y') =
  moveto x y;
  lineto x' y';;

let dessine_enveloppe nuage =
  let dessin a b =
    match repartition a b nuage with
    | (0, _, _) | (_, _, 0) -> segment a b
    | _ -> () in
  dessine_couples dessin nuage;;

(** Question 5 *)
let intersecte (a, b) (c, d) =
  let opposes = function
  | Gauche, Droite | Droite, Gauche -> true
  | _ -> false in
  opposes (position a b c, position a b d)
  && opposes (position c d a, position c d b);;
  
let intersecte_un seg liste =
  exists (intersecte seg) liste;;

let dessine_sans_couper () =
  let memoire = ref [] in
  fun a b ->
    let seg = (a, b) in
    if not (intersecte_un seg !memoire) then begin
      memoire := seg :: !memoire;
      segment a b
    end;;

let triangulation nuage =
  nettoyer ();
  dessine_couples (dessine_sans_couper ()) nuage;;

(** Question 6 *)
let triangulation2 nuage =
  let rec dessine_couples_2 dessin = function
  | [] -> ()
  | hd::tl -> dessine_couples dessin tl; do_list (dessin hd) tl in
  nettoyer ();
  dessine_couples_2 (dessine_sans_couper ()) nuage;;

(** Question 7 *)
let trie_longueur segments =
  let long2 (a, b) =
    let (x, y) = vect a b in
    x * x + y * y in
  sort__sort (fun a b -> long2 a <= long2 b) segments;;

let triangulation3 nuage =
  let rec liste_segments = function
  | [] -> []
  | x::xs -> map (fun y -> (x, y)) xs @ liste_segments xs in
  let dessin = dessine_sans_couper () in
  nettoyer ();
  do_list (fun (a, b) -> dessin a b)
    (trie_longueur (liste_segments nuage));;

(** Question 8 *)
let last_key () =
  if (wait_next_event [Poll; Key_pressed]).keypressed
  then read_key () else `x`;;

let size = xlen;;

let moyenner tab =
  let moyenne_locale x =
    let get i = tab.((i + size) mod size) in
    (get (x-1) +. get x +. get(x+1)) /. 3. in
  for i = 0 to size - 1 do
    tab.(i) <- moyenne_locale i
  done;;

let surface = make_vect size (float_of_int (ylen / 2)) ;;

let evolue () = moyenner surface;;

let dessine_surface () =
  for i = 0 to size - 1 do
    let y = int_of_float surface.(i) in
    moveto i 0;
    set_color black;
    lineto i y;
    set_color white;
    lineto i ylen
  done;;

let rec tourner evolue =
  evolue ();
  dessine_surface ();
  match last_key () with
  | `q` -> ()
  | ` ` ->
      let (x, y) = mouse_pos () in
      if x < size && x >= 0 then
        surface.(x) <- float_of_int y;
      tourner evolue;
  | _ -> tourner evolue;;

