(** Question 1 *)
let valide str =
  let valeur = function
    | `(` -> 1 | `)` -> (-1) | _ -> 0 in
  let rec scan n i =
    if i = string_length str then n = 0
    else n >= 0 && scan (n + valeur str.[i]) (i + 1) in
  scan 0 0 ;;

(** Question 3 : Addition : ,>,[-<+>]<. *)
(** Question 4 : Multiplication : ,>,[-<[-<+<+>>]<[->+<]>>]<<<. *)

type prog == instr list
and instr =
  | Move of int
  | Change of int
  | Read
  | Print
  | Loop of prog
;;


(** Question 6 *)
exception Erreur of int;;

let bf_parse str =
  let prefix ++ x (xs, y) = (x :: xs, y) in
  let rec parse i =
    if i = string_length str then ([], i)
    else match str.[i] with
    | `.` -> Print ++ parse (i + 1)
    | `,` -> Read ++ parse (i + 1)
    | `>` -> Move 1 ++ parse (i + 1)
    | `<` -> Move (-1) ++ parse (i + 1)
    | `+` -> Change 1 ++ parse (i + 1)
    | `-` -> Change (-1) ++ parse (i + 1)
    | `]` -> ([], i)
    | `[` ->
      let (loop, i') = parse (i + 1) in
      if i' = string_length str then raise (Erreur i)
      else Loop loop ++ parse (i' + 1)
    | _ -> parse (i + 1)
  in
  let (prog, fin) = parse 0 in
  if fin < string_length str then raise (Erreur fin)
  else prog
;;


(** Question 7 *)
let bf_eval prog =
  let taille = 100 in
  let memoire = make_vect taille 0 in
  let ptr = ref 50 in
  let rec eval_prog prog = do_list eval prog
  and eval = function
  | Move n -> ptr := !ptr + n
  | Change n -> memoire.(!ptr) <- memoire.(!ptr) + n
  | Read -> memoire.(!ptr) <- read_int ()
  | Print -> print_int memoire.(!ptr); print_newline ()
  | Loop loop -> while memoire.(!ptr) <> 0 do eval_prog loop done
  in eval_prog prog
;;
