# Personal page

> **Gabriel SCHERER**
> [`<gabriel.scherer@gmail.com`](mailto:gabriel.scherer@gmail.com)

This repository contains web pages which mainly act as a personal portfolio.

These static web pages are graciously generated and hosted by [*GitLab*](https://gitlab.com)
under the following URL: [`http://gasche.gitlab.io/`](http://gasche.gitlab.io/).